package com.example.cryptobynex.fragments.books

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.models.BookDto
import com.example.domain.usecase.GetBooksUseCase
import kotlinx.coroutines.launch

class BookViewModel(private val bookUseCase: GetBooksUseCase) : ViewModel() {

    private val _booksLiveData = MutableLiveData<List<BookDto>>()
    val bookLiveData: LiveData<List<BookDto>> = _booksLiveData

    fun getBooks(count: Int) {
        viewModelScope.launch {
            _booksLiveData.value = bookUseCase.execute(count)
        }
    }
}