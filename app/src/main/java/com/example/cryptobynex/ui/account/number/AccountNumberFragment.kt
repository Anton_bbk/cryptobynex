package com.example.cryptobynex.fragments.profile.number

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.cryptobynex.databinding.FragmentAccountNumberBinding
import com.example.cryptobynex.fragments.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class AccountNumberFragment : BaseFragment<FragmentAccountNumberBinding>() {

    private val viewModel by viewModel<AccountNumberViewModel>()

    override fun createViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentAccountNumberBinding =
        FragmentAccountNumberBinding.inflate(inflater, container, false)

    override fun FragmentAccountNumberBinding.onBindView(saveInstanceState: Bundle?) {

        submit.setOnClickListener {
            viewModel.editNumber(editTextField.text.toString())
            navController.popBackStack()
        }
    }
}