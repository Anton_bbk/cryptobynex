package com.example.cryptobynex.ui.account

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.cryptobynex.databinding.FragmentAccountBinding
import com.example.domain.models.AccountDivider
import com.example.domain.models.AccountItem
import com.example.cryptobynex.fragments.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class AccountFragment : BaseFragment<FragmentAccountBinding>() {

    private val viewModel by viewModel<AccountViewModel>()

    private val adapter = AccountAdapter()

    override fun createViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentAccountBinding = FragmentAccountBinding.inflate(inflater, container, false)

    override fun FragmentAccountBinding.onBindView(saveInstanceState: Bundle?) {

        items.adapter = adapter
        viewModel.getUserInfo()
        viewModel.userInfoLiveData.observe(viewLifecycleOwner) { userAccount ->
            adapter.submitList(
                listOf(
                    AccountDivider,
                    AccountItem("name", userAccount.name) {
                        navController.navigate(AccountFragmentDirections.navigationToAccountNameFragment())
                    },
                    AccountDivider,
                    AccountItem("age", userAccount.age.toString()) {
                        navController.navigate(AccountFragmentDirections.navigationToAccountAgeFragment())
                    },
                    AccountDivider,
                    AccountItem("number", userAccount.number) {
                        navController.navigate(AccountFragmentDirections.navigationToAccountNumberFragment())
                    },
                    AccountDivider,
                    AccountItem("email", userAccount.email) {
                        navController.navigate(AccountFragmentDirections.navigationToAccountEmailFragment())
                        val intent = Intent(Intent.ACTION_VIEW)
                        intent.setData(Uri.parse("https://www.${userAccount.email}"))
                        startActivity(intent)
                    },
                    AccountDivider
                )
            )
        }
    }
}