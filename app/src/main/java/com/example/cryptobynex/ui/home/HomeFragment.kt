package com.example.cryptobynex.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.cryptobynex.databinding.FragmentHomeBinding
import com.example.cryptobynex.fragments.base.BaseFragment

class HomeFragment : BaseFragment<FragmentHomeBinding>() {
    override fun createViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentHomeBinding = FragmentHomeBinding.inflate(inflater, container, false)

    override fun FragmentHomeBinding.onBindView(saveInstanceState: Bundle?) {

        books.setOnClickListener {
            navController.navigate(HomeFragmentDirections.navigateToBooksFragment())
        }

        btCrypto.setOnClickListener {
            navController.navigate(HomeFragmentDirections.navigateToCryptoFragment())
        }

        btYouTube.setOnClickListener {
            navController.navigate(HomeFragmentDirections.navigateToYouTubeFragment())
        }


    }
}
