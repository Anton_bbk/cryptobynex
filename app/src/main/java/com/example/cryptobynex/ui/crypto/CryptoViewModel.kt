package com.example.cryptobynex.ui.crypto

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.domain.models.CryptoItem
import com.example.domain.usecase.GetCryptoInformationUseCase

class CryptoViewModel(private val getCryptoInformationUseCase: GetCryptoInformationUseCase) :
    ViewModel() {

    private val _getCryptoLiveData = MutableLiveData<CryptoItem>()
    val getCryptoLiveData: LiveData<CryptoItem> = _getCryptoLiveData

    fun getCrypto(): List<CryptoItem> {
        return getCryptoInformationUseCase.execute()
    }

}