package com.example.cryptobynex.fragments.profile.email

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.cryptobynex.databinding.FragmentAccountEmailBinding
import com.example.cryptobynex.fragments.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class AccountEmailFragment : BaseFragment<FragmentAccountEmailBinding>() {

    private val viewModel by viewModel<AccountEmailViewModel>()

    override fun createViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentAccountEmailBinding =
        FragmentAccountEmailBinding.inflate(inflater, container, false)

    override fun FragmentAccountEmailBinding.onBindView(saveInstanceState: Bundle?) {

        submit.setOnClickListener {
            viewModel.editEmail(editTextField.text.toString())
            navController.popBackStack()
        }
    }
}