//package com.example.cryptobynex.ui.youtube
//
//import android.os.Bundle
//import android.view.LayoutInflater
//import android.view.View
//import android.view.ViewGroup
//import androidx.fragment.app.Fragment
//import androidx.lifecycle.Observer
//import androidx.lifecycle.ViewModelProvider
//import com.example.cryptobynex.R
//
//class YouTubeFragment: Fragment() {
//
//    private lateinit var homeViewModel: HomeViewModel
//
//    override fun onCreateView(
//        inflater: LayoutInflater,
//        container: ViewGroup?,
//        savedInstanceState: Bundle?
//    ): View? {
//        homeViewModel =
//            ViewModelProvider(this).get(HomeViewModel::class.java)
//        val root = inflater.inflate(R.layout.fragment_home, container, false)
//
//        homeViewModel.channel.observe(viewLifecycleOwner, Observer {
//            if (it != null && it.items.isNotEmpty()) {
//                it.items.forEach { channel ->
//
//                }
//            }
//        })
//        return root
//    }
//}

package com.example.cryptobynex.ui.youtube

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.cryptobynex.databinding.FragmentYoutubeBinding
import com.example.cryptobynex.fragments.base.BaseFragment

class YouTubeFragment:BaseFragment<FragmentYoutubeBinding>() {
    override fun createViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentYoutubeBinding = FragmentYoutubeBinding.inflate(inflater,container,false)

    override fun FragmentYoutubeBinding.onBindView(saveInstanceState: Bundle?) {

    }
}
