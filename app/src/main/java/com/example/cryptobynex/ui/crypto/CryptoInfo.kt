package com.example.cryptobynex.ui.crypto

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.cryptobynex.databinding.FragmentCryptoInfoBinding
import com.example.cryptobynex.fragments.base.BaseFragment

class CryptoInfo : BaseFragment<FragmentCryptoInfoBinding>() {
    override fun createViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentCryptoInfoBinding = FragmentCryptoInfoBinding.inflate(inflater, container, false)

    override fun FragmentCryptoInfoBinding.onBindView(saveInstanceState: Bundle?) {

    }
}