package com.example.cryptobynex.fragments.profile.number

import androidx.lifecycle.ViewModel
import com.example.domain.usecase.EditNumberUseCase

class AccountNumberViewModel(private val editNumberUseCase: EditNumberUseCase):ViewModel() {

    fun editNumber(number: String) {
        editNumberUseCase.execute(param = number)
    }
}