package com.example.cryptobynex.fragments.profile.name

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.cryptobynex.databinding.FragmentAccountNameBinding
import com.example.cryptobynex.fragments.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class AccountNameFragment : BaseFragment<FragmentAccountNameBinding>() {

    private val viewModel by viewModel<AccountNameViewModel>()

    override fun createViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentAccountNameBinding =
        FragmentAccountNameBinding.inflate(inflater, container, false)

    override fun FragmentAccountNameBinding.onBindView(saveInstanceState: Bundle?) {

        submit.setOnClickListener {
            viewModel.editName(editTextField.text.toString())
            navController.popBackStack()
        }
    }
}
