package com.example.cryptobynex.fragments.profile.age

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.cryptobynex.databinding.FragmentAccountAgeBinding
import com.example.cryptobynex.fragments.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class AccountAgeFragment : BaseFragment<FragmentAccountAgeBinding>() {

    private val viewModel by viewModel<AccountAgeViewModel>()

    override fun createViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentAccountAgeBinding = FragmentAccountAgeBinding.inflate(inflater, container, false)

    override fun FragmentAccountAgeBinding.onBindView(saveInstanceState: Bundle?) {

        numberPicker.minValue = 12
        numberPicker.maxValue = 100
        numberPicker.wrapSelectorWheel = true

        submit.setOnClickListener {
            viewModel.editAge(numberPicker.value)
            navController.popBackStack()
        }
    }
}