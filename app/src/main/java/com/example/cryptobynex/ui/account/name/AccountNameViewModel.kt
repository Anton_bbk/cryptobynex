package com.example.cryptobynex.fragments.profile.name

import androidx.lifecycle.ViewModel
import com.example.domain.usecase.EditNameUseCase

class AccountNameViewModel(private val editNameUseCase: EditNameUseCase) : ViewModel() {

    fun editName(name: String) {
        editNameUseCase.execute(param = name)
    }
}