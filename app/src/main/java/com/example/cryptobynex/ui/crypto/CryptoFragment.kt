package com.example.cryptobynex.ui.crypto

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.cryptobynex.R
import com.example.cryptobynex.databinding.FragmentCryptoBinding
import com.example.domain.models.CryptoItem
import com.example.cryptobynex.fragments.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class CryptoFragment : BaseFragment<FragmentCryptoBinding>() {

//    private val viewModel by viewModel<CryptoViewModel>()

    private val adapter = CryptoAdapter()

    override fun createViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentCryptoBinding = FragmentCryptoBinding.inflate(inflater, container, false)

    override fun FragmentCryptoBinding.onBindView(saveInstanceState: Bundle?) {

        items.adapter = adapter
//        viewModel.getCrypto()
//        viewModel.getCryptoLiveData.observe(viewLifecycleOwner) {
        adapter.submitList(
            listOf(
                CryptoItem(R.drawable.crypto_bitcoin, "Bitcoin", {navController.navigate(CryptoFragmentDirections.navigateToCryptoInfo())}),
                CryptoItem(R.drawable.crypto_ethereum, "Ethereum", {}),
                CryptoItem(R.drawable.crypto_litecoin, "Litecoin", {}),
                CryptoItem(R.drawable.crypto_ripple, "Ripple", {}),
                CryptoItem(R.drawable.crypto_doge, "Doge", {}),
                CryptoItem(R.drawable.crypto_solana, "Solana", {}),
                CryptoItem(R.drawable.crypto_avalanche, "Avalanche", {}),
                CryptoItem(R.drawable.crypto_bnb, "Binance coin", {}),
                CryptoItem(R.drawable.crypto_chainlink, "Chainlink", {}),
                CryptoItem(R.drawable.crypto_polkadot, "Polkadot", {}),
                CryptoItem(R.drawable.crypto_cardano, "Cardano", {}),
                CryptoItem(R.drawable.crypto_maker, "Maker", {}),
                CryptoItem(R.drawable.crypto_polygon, "Polygon", {}),
                CryptoItem(R.drawable.crypto_stellar, "Stellar", {}),
                CryptoItem(R.drawable.crypto_tron, "Tron", {}),
                CryptoItem(R.drawable.crypto_uniswap, "Uniswap", {})
            )
        )

    }
}
//}
