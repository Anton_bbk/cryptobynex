package com.example.cryptobynex.fragments.books

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.cryptobynex.databinding.FragmentBooksBinding
import com.example.cryptobynex.fragments.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class BooksFragment : BaseFragment<FragmentBooksBinding>() {

    private val viewModel by viewModel<BookViewModel>()

    private val bookAdapter = BookAdapter()

    override fun createViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentBooksBinding = FragmentBooksBinding.inflate(inflater, container, false)

    override fun FragmentBooksBinding.onBindView(saveInstanceState: Bundle?) {
        viewModel.getBooks(6)
        bookList.adapter = bookAdapter

        viewModel.bookLiveData.observe(viewLifecycleOwner) { books ->
            bookAdapter.submitList(books)
        }
    }
}