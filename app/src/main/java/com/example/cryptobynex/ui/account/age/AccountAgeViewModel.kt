package com.example.cryptobynex.fragments.profile.age

import androidx.lifecycle.ViewModel
import com.example.domain.usecase.EditAgeUseCase

class AccountAgeViewModel(private val editAgeUseCase: EditAgeUseCase) : ViewModel() {

    fun editAge(age: Int) {
        editAgeUseCase.execute(param = age)
    }
}