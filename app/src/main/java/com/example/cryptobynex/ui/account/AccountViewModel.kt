package com.example.cryptobynex.ui.account

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.domain.models.UserAccountInfo
import com.example.domain.usecase.GetUserInformationUseCase

class AccountViewModel(private val getUserInformationUseCase: GetUserInformationUseCase) : ViewModel() {

    private val _userInfoLiveData = MutableLiveData<UserAccountInfo>()
    val userInfoLiveData: LiveData<UserAccountInfo> = _userInfoLiveData

    fun getUserInfo() {
        val result = getUserInformationUseCase.execute()
        _userInfoLiveData.value = result
    }
}