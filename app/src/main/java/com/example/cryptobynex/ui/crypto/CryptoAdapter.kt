package com.example.cryptobynex.ui.crypto

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.viewbinding.ViewBinding
import com.example.cryptobynex.databinding.CryptoItemBinding
import com.example.domain.models.CryptoItem
import com.example.cryptobynex.fragments.base.BaseViewHolder

class CryptoAdapter :
    ListAdapter<Any, BaseViewHolder<ViewBinding, Any>>(
        object : DiffUtil.ItemCallback<Any>() {
            override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean = when {
                oldItem is CryptoItem && newItem is CryptoItem -> oldItem.title == newItem.title
                else -> false
            }

            @SuppressLint("DiffUtilEquals")
            override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean = when {
                oldItem is CryptoItem && newItem is CryptoItem -> oldItem == newItem
                else -> false
            }
        }
    ) {
    override fun getItemViewType(position: Int): Int = when (getItem(position)) {
        is CryptoItem -> CRYPTO_ITEM_TYPE
        else -> throw IllegalArgumentException(
            "CryptoAdapter can't handle item" + getItem(position)
        )
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<ViewBinding, Any> = when (viewType) {
        CRYPTO_ITEM_TYPE -> CryptoViewHolder(parent) as BaseViewHolder<ViewBinding, Any>
        else -> throw IllegalArgumentException("CryptoAdapter can't handle item $viewType viewType")
    }

    override fun onBindViewHolder(holder: BaseViewHolder<ViewBinding, Any>, position: Int) =
        holder.handleItem(getItem(position))

    companion object {
        private const val CRYPTO_ITEM_TYPE = 999
    }
}

private class CryptoViewHolder(parent: ViewGroup) :
    BaseViewHolder<CryptoItemBinding, CryptoItem>(
        CryptoItemBinding.inflate(
            LayoutInflater.from(
                parent.context
            ), parent, false
        )
    ) {
    override fun CryptoItemBinding.bind(value: CryptoItem) {
        title.text = value.title
        imageCrypto.setImageResource(value.imageCrypto)
        itemView.setOnClickListener{
            value.action()
        }
    }
}