package com.example.cryptobynex.ui.account

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.viewbinding.ViewBinding
import com.example.cryptobynex.databinding.AccountDividerBinding
import com.example.cryptobynex.databinding.AccountItemBinding
import com.example.domain.models.AccountDivider
import com.example.domain.models.AccountItem
import com.example.cryptobynex.fragments.base.BaseViewHolder

class AccountAdapter :
    ListAdapter<Any, BaseViewHolder<ViewBinding, Any>>(
        object : DiffUtil.ItemCallback<Any>() {
            override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean = when {
                oldItem is AccountItem && newItem is AccountItem -> oldItem.title == newItem.title
                oldItem is AccountDivider && newItem is AccountDivider -> true
                else -> false
            }

            @SuppressLint("DiffUtilEquals")
            override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean = when {
                oldItem is AccountItem && newItem is AccountItem -> oldItem == newItem
                oldItem is AccountDivider && newItem is AccountDivider -> true
                else -> false
            }
        }
    ) {
    override fun getItemViewType(position: Int): Int = when (getItem(position)) {
        is AccountItem -> ACCOUNT_ITEM_TYPE
        is AccountDivider -> ACCOUNT_DIVIDER_TYPE
        else -> throw IllegalArgumentException(
            "AccountAdapter can't handle item" + getItem(position)
        )
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<ViewBinding, Any> = when (viewType) {
        ACCOUNT_ITEM_TYPE -> AccountViewHolder(parent) as BaseViewHolder<ViewBinding, Any>
        ACCOUNT_DIVIDER_TYPE -> AccountDividerViewHolder(parent) as BaseViewHolder<ViewBinding, Any>
        else -> throw IllegalArgumentException("AccountAdapter can't handle item $viewType viewType")
    }

    override fun onBindViewHolder(holder: BaseViewHolder<ViewBinding, Any>, position: Int) =
        holder.handleItem(getItem(position))

    companion object {
        private const val ACCOUNT_ITEM_TYPE = 999
        private const val ACCOUNT_DIVIDER_TYPE = 998
    }
}

private class AccountViewHolder(parent: ViewGroup) :
    BaseViewHolder<AccountItemBinding, AccountItem>(
        AccountItemBinding.inflate(
            LayoutInflater.from(
                parent.context
            ), parent, false
        )
    ) {
    override fun AccountItemBinding.bind(value: AccountItem) {
        title.text = value.title
        subTitle.text = value.subtitle
        itemView.setOnClickListener{
            value.action()
        }
    }
}

private class AccountDividerViewHolder(parent: ViewGroup) :
    BaseViewHolder<AccountDividerBinding, AccountDivider>(
        AccountDividerBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )