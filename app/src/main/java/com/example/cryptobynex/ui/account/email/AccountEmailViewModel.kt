package com.example.cryptobynex.fragments.profile.email

import androidx.lifecycle.ViewModel
import com.example.domain.usecase.EditEmailUseCase

class AccountEmailViewModel(private val editEmailUseCase: EditEmailUseCase) : ViewModel() {

    fun editEmail(email: String) {
        editEmailUseCase.execute(param = email)
    }
}