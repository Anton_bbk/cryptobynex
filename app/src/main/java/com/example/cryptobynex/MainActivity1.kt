package com.example.cryptobynex

import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import com.google.android.youtube.player.YouTubeBaseActivity
import com.google.android.youtube.player.YouTubeInitializationResult
import com.google.android.youtube.player.YouTubePlayer
import com.google.android.youtube.player.YouTubePlayerView

class MainActivity1 : YouTubeBaseActivity() {

    val VIDEO_ID = "Kt0O05WMu6I"
    val YOUTUBE_API_KEY = "AIzaSyABeg4ANV0xMt4CX0ZSQKzFr6vmKlbv9k8"

    private lateinit var youTubePlayer: YouTubePlayerView
    private lateinit var btPlay: Button

    lateinit var youTubePlayerInit: YouTubePlayer.OnInitializedListener


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main1)

        youTubePlayer = findViewById(R.id.youTubePlayer)
        btPlay = findViewById(R.id.btPlay)

        youTubePlayerInit = object : YouTubePlayer.OnInitializedListener {
            override fun onInitializationSuccess(
                p0: YouTubePlayer.Provider?,
                p1: YouTubePlayer?,
                p2: Boolean
            ) {
                p1?.loadVideo(VIDEO_ID)
            }

            override fun onInitializationFailure(
                p0: YouTubePlayer.Provider?,
                p1: YouTubeInitializationResult?
            ) {
                Toast.makeText(applicationContext, "Failed", Toast.LENGTH_SHORT).show()
            }
        }

        btPlay.setOnClickListener {
            youTubePlayer.initialize(YOUTUBE_API_KEY, youTubePlayerInit)
        }

    }
}