package com.example.cryptobynex.di

import com.example.domain.usecase.*
import org.koin.dsl.module

val domainModule = module {

    factory<GetCryptoInformationUseCase> { GetCryptoInformationUseCase(get()) }

    factory<EditAgeUseCase> { EditAgeUseCase(get()) }

    factory<EditEmailUseCase> { EditEmailUseCase(get()) }

    factory<EditNameUseCase> { EditNameUseCase(get()) }

    factory<EditNumberUseCase> { EditNumberUseCase(get()) }

    factory<GetUserInformationUseCase> { GetUserInformationUseCase(get()) }

    factory<GetBooksUseCase> { GetBooksUseCase(get()) }
}