package com.example.cryptobynex.di

import com.example.cryptobynex.fragments.books.BookViewModel
import com.example.cryptobynex.fragments.profile.age.AccountAgeViewModel
import com.example.cryptobynex.fragments.profile.email.AccountEmailViewModel
import com.example.cryptobynex.fragments.profile.name.AccountNameViewModel
import com.example.cryptobynex.fragments.profile.number.AccountNumberViewModel
import com.example.cryptobynex.ui.account.AccountViewModel
import com.example.cryptobynex.ui.crypto.CryptoViewModel
import com.example.cryptobynex.ui.home.HomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    viewModel<CryptoViewModel> {
        CryptoViewModel(get())
    }
    viewModel<AccountViewModel> {
        AccountViewModel(get())
    }
    viewModel<AccountAgeViewModel> {
        AccountAgeViewModel(get())
    }

    viewModel<AccountEmailViewModel> {
        AccountEmailViewModel(get())
    }

    viewModel<AccountNameViewModel> {
        AccountNameViewModel(get())
    }

    viewModel<AccountNumberViewModel> {
        AccountNumberViewModel(get())
    }

    viewModel<HomeViewModel> {
        HomeViewModel()
    }
    viewModel<BookViewModel> {
        BookViewModel(get())
    }
}