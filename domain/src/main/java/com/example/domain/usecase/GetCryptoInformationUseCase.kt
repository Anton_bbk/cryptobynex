package com.example.domain.usecase

import com.example.domain.base.UseCase
import com.example.domain.models.CryptoItem
import com.example.domain.repository.CryptoRepository

class GetCryptoInformationUseCase (private val cryptoRepository: CryptoRepository) :
    UseCase<Unit, List<CryptoItem>> {
    override fun execute(param: Unit?): List<CryptoItem> {
        return cryptoRepository.GetCryptoItem()
    }
}