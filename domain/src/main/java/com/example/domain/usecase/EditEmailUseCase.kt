package com.example.domain.usecase

import com.example.domain.base.UseCase
import com.example.domain.repository.UserAccountRepository

class EditEmailUseCase(private val userAccountRepository: UserAccountRepository) :
    UseCase<String, Unit> {
    override fun execute(param: String?) {
        return userAccountRepository.editAccountEmail(param!!)
    }
}
