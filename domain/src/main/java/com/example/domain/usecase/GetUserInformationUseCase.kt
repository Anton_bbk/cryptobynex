package com.example.domain.usecase

import com.example.domain.base.UseCase
import com.example.domain.models.UserAccountInfo
import com.example.domain.repository.UserAccountRepository

class GetUserInformationUseCase(private val userAccountRepository: UserAccountRepository) :
    UseCase<Unit, UserAccountInfo> {
    override fun execute(param: Unit?): UserAccountInfo {
        return userAccountRepository.getUserAccountInfo()
    }
}