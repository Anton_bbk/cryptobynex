package com.example.domain.usecase

import com.example.domain.base.UseCase
import com.example.domain.repository.UserAccountRepository

class EditAgeUseCase(private val userAccountRepository: UserAccountRepository) :
    UseCase<Int, Unit> {
    override fun execute(param: Int?) {
        return userAccountRepository.editAccountAge(param!!)
    }
}
