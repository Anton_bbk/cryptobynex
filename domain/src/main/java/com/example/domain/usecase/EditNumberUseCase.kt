package com.example.domain.usecase

import com.example.domain.base.UseCase
import com.example.domain.repository.UserAccountRepository

class EditNumberUseCase(private val userAccountRepository: UserAccountRepository) :
    UseCase<String, Unit> {
    override fun execute(param: String?) {
        userAccountRepository.editAccountNumber(param!!)
    }
}