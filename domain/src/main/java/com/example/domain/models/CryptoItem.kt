package com.example.domain.models

data class CryptoItem(
    val imageCrypto: Int,
    val title: String,
    val action: ()->Unit
)
