package com.example.domain.models

data class AccountItem(
    val title: String,
    val subtitle: String,
    val action: ()->Unit
)
