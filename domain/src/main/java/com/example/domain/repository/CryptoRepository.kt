package com.example.domain.repository

import com.example.domain.models.CryptoItem

interface CryptoRepository {

    fun GetCryptoItem(): List<CryptoItem>
}