package com.example.domain.repository

import com.example.domain.models.UserAccountInfo

interface UserAccountRepository {

    fun getUserAccountInfo(): UserAccountInfo

    fun editAccountNumber(number: String)

    fun editAccountName(name: String)

    fun editAccountAge(age: Int)

    fun editAccountEmail(email: String)
}