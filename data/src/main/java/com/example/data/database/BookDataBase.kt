package com.example.data.storage.database

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [BookEntity::class], version = 1, exportSchema = true)
abstract class BookDataBase : RoomDatabase() {
    abstract fun bookDao(): BookDao
}