package com.example.data.crypto

import com.example.domain.models.CryptoItem

interface CryptoStorage {
    fun getCryptoItem(): CryptoItem
}