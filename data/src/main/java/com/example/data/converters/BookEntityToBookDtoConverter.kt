package com.example.data.converters

import com.example.data.storage.database.BookEntity
import com.example.domain.base.Converter
import com.example.domain.models.BookDto

class BookEntityToBookDtoConverter : Converter<BookEntity, BookDto> {
    override fun invoke(params: BookEntity): BookDto {
        return BookDto(
            id = params.id ?: -1,
            title = params.title ?: "",
            author = params.author ?: "",
            genre = params.genre ?: "",
            description = params.description ?: "",
            isbn = params.isbn ?: "",
            image = params.image ?: "",
            published = params.published ?: "",
            publisher = params.publisher ?: ""
        )
    }
}