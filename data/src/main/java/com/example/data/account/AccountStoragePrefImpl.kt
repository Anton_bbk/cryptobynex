package com.example.data.storage.profile

import android.content.Context
import androidx.core.content.edit
import com.example.domain.models.UserAccountInfo

class AccountStoragePrefImpl(context: Context) : AccountStorage {

    private val sharedPref = context.getSharedPreferences(ACCOUNT_PREF_NAME, Context.MODE_PRIVATE)

    override fun getAccountInfo(): UserAccountInfo {
        return UserAccountInfo(
            number = sharedPref.getString(NUMBER_KEY, DEFAULT_NUMBER) ?: EMPTY_STRING,
            name = sharedPref.getString(NAME_KEY, DEFAULT_NAME) ?: EMPTY_STRING,
            email = sharedPref.getString(EMAIL_KEY, DEFAULT_EMAIL) ?: EMPTY_STRING,
            age = sharedPref.getInt(AGE_KEY, DEFAULT_AGE)
        )
    }

    override fun editAccountNumber(number: String) {
        sharedPref.edit {
            putString(NUMBER_KEY, number)
        }
    }

    override fun editAccountName(name: String) {
        sharedPref.edit {
            putString(NAME_KEY, name)
        }
    }

    override fun editAccountAge(age: Int) {
        sharedPref.edit {
            putInt(AGE_KEY, age)
        }
    }

    override fun editAccountEmail(email: String) {
        sharedPref.edit {
            putString(EMAIL_KEY, email)
        }
    }

    companion object {

        const val ACCOUNT_PREF_NAME = "ACCOUNT_PREF_NAME"

        const val NUMBER_KEY = "NUMBER_KEY"
        const val NAME_KEY = "NAME_KEY"
        const val AGE_KEY = "AGE_KEY"
        const val EMAIL_KEY = "EMAIL_KEY"

        const val DEFAULT_NAME = "profile name"
        const val DEFAULT_NUMBER = "+375299399992"
        const val DEFAULT_EMAIL = "email@bynex.io"
        const val DEFAULT_AGE = 27

        const val EMPTY_STRING = ""
    }
}