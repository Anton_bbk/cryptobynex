package com.example.data.storage.profile

import com.example.domain.models.UserAccountInfo

interface AccountStorage {
    fun getAccountInfo(): UserAccountInfo

    fun editAccountNumber(number: String)

    fun editAccountName(name: String)

    fun editAccountAge(age: Int)

    fun editAccountEmail(email: String)
}