package com.example.data.repo

import com.example.data.converters.*
import com.example.data.storage.database.DataBaseBookStorage
import com.example.data.storage.network.NetworkStorage
import com.example.domain.models.BookDto
import com.example.domain.repository.NetworkRepository

class NetworkRepositoryImpl(
    private val networkStorage: NetworkStorage,
    private val dataBaseBookStorage: DataBaseBookStorage,
    private val bookToBookDtoConverter: BookToBookDtoConverter,
    private val bookToBookEntityConverter: BookToBookEntityConverter,
    private val bookEntityToBookDtoConverter: BookEntityToBookDtoConverter,
) : NetworkRepository {
    override suspend fun getBooks(count: Int): List<BookDto> {
        try {
            val response = networkStorage.getBooks(count)
            if (response.isSuccessful) {
                response.body()?.let { baseResponse ->
                    val books = mutableListOf<BookDto>()
                    baseResponse.data?.map { book ->
                        dataBaseBookStorage.insertBook(bookToBookEntityConverter.invoke(book))
                        books.add(bookToBookDtoConverter.invoke(book))
                    }
                    return books
                }
            } else {
                val books = mutableListOf<BookDto>()
                dataBaseBookStorage.getBooks().map { bookEntity ->
                    books.add(bookEntityToBookDtoConverter.invoke(bookEntity))
                }
                return books
            }
        } catch (e: Exception) {
            val books = mutableListOf<BookDto>()
            dataBaseBookStorage.getBooks().map { bookEntity ->
                books.add(bookEntityToBookDtoConverter.invoke(bookEntity))
            }
            return books
        }
        return emptyList()
    }
}