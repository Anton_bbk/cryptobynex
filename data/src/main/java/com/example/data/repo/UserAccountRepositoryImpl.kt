package com.example.data.repo

import com.example.data.storage.profile.AccountStorage
import com.example.domain.models.UserAccountInfo
import com.example.domain.repository.UserAccountRepository

class UserAccountRepositoryImpl(private val accountStorage: AccountStorage) :
    UserAccountRepository {

    override fun getUserAccountInfo(): UserAccountInfo {
        return accountStorage.getAccountInfo()
    }

    override fun editAccountNumber(number: String) {
       return accountStorage.editAccountNumber(number)
    }

    override fun editAccountName(name: String) {
        return accountStorage.editAccountName(name)
    }

    override fun editAccountAge(age: Int) {
        return accountStorage.editAccountAge(age)
    }

    override fun editAccountEmail(email: String) {
        return accountStorage.editAccountEmail(email)
    }
}